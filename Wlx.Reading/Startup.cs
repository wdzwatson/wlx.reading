﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Wlx.Reading.Startup))]
namespace Wlx.Reading
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
